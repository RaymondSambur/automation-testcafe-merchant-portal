var faker = require('faker')

class DefaultData {
    constructor() {
        this.registerApp = {
            brandName: faker.commerce.productName(),
            ownerName: faker.name.firstName() + " " + faker.name.lastName(),
            province: "JAWA BARAT",
            businessCategory: "Entertainment",
            email: faker.name.firstName().toLowerCase() + faker.name.lastName().toLowerCase() + "@mailinator.com",
            password: "Password123!",
            passwordConfirm: "Password123!",
            businessChannel: "Offline",
            danaProduct: "Yes",
            inputDanaProduct: "DANA Pulsa"
        }
        this.loginApp = {
            email: "blake@mailinator.com",
            password: "Password123!"
        }
        this.businessInfoApp = {
            businessProfile: {
                businessName: faker.company.companyName(),
                brandName: faker.commerce.productName(),
                businessType: "PT",
                businessSizeType: "Usaha Mikro",
                businessCategory: "FAST FOOD RESTAURANTS",
                filePath: "/Users/samburr/Downloads/587777-2.png",
                ownerName: faker.name.firstName() + " " + faker.name.lastName(),
                ktpNumber: "123456789",
                financeName: faker.name.firstName() + " " + faker.name.lastName(),
                financePhoneNumber: "8123456789",
                financeEmail: faker.name.firstName().toLowerCase() + faker.name.lastName().toLowerCase() + "@mailinator.com",
                stores: "1"
            },
            officeAddress: {
                registeredCountry: "INDONESIA",
                registeredProvince: "SULAWESI UTARA",
                registeredCity: "KOTA MANADO",
                registeredArea: "PAAL DUA",
                registeredAddress: "Default Address",
                registeredPostCode: "12345",
                officeCountry: "INDONESIA",
                officeProvince: "SULAWESI UTARA",
                officeCity: "KOTA MANADO",
                officeArea: "PAAL DUA",
                officeAddress: "Default Address",
                officePostCode: "12345"
            },
            contactInfo: {
                contactName: faker.name.firstName() + " " + faker.name.lastName(),
                contactMobileNumber: "8123456789",
                contactTelephone: "8123456789",
                contactEmailAddress: faker.name.firstName().toLowerCase() + faker.name.lastName().toLowerCase() + "@mailinator.com"
            },
            settleAccountInfo: {
                bankName: "014 BCA BANK CENTRAL ASIA",
                subsidiaryBankName: "Default Subsidiary Bank Name",
                accountNumber: "123456789",
                cardholderName: faker.name.firstName() + " " + faker.name.lastName()
            }
        }
        this.legalApp = {
            aktaAnggaran: "/Users/samburr/Downloads/Test005.pdf",
            siup: "/Users/samburr/Downloads/Test005.pdf",
            npwp: "/Users/samburr/Downloads/Test005.pdf",
            rekeningKoran: "/Users/samburr/Downloads/Test005.pdf",
            ktp: "/Users/samburr/Downloads/Test005.pdf",
            nib: "/Users/samburr/Downloads/Test005.pdf"
        }
    }
}
export default new DefaultData()