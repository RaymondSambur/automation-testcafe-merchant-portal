import { t } from "testcafe"
import loginPageLocator from "../../page-selector/cms/login-page-locator"

class Login {
    async inputEmail(email) {
        await t.typeText(loginPageLocator.inputEmail, email)
    }

    async inputPassword(password) {
        await t.typeText(loginPageLocator.inputPassword, password)
    }

    async clickRememberMeButton() {
        await t.click(loginPageLocator.rememberMeButton)
    }

    async clickHideShowPasswordButton() {
        await t.click(loginPageLocator.hideShowButton)
    }

    async clickLoginButton() {
        await t.click(loginPageLocator.loginButton)
    }
}