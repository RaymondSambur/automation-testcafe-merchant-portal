import loginLocator from "../../page-selector/app/login-page-locator"
import { t } from "testcafe"

class Login {
    async inputEmailAddress(email) {
        await t.typeText(loginLocator.emailInput, email)
    }

    async inputPassword(password) {
        await t.typeText(loginLocator.passwordInput, password)
    }

    async clickForgotPasswordBtn() {
        await t.click(loginLocator.forgotPassswordBtn)
    }

    async clickRememberMeBtn() {
        await t.click(loginLocator.rememberMeBtn)
    }

    async clickLoginBtn() {
        await t.click(loginLocator.loginBtn)
    }

    async normalLogin(data) {
        this.inputEmailAddress(data.email)
        this.inputPassword(data.password)
        this.clickLoginBtn()
    }
}

export default new Login()