import { t } from "testcafe"
import businessInfoPageLocator from '../../page-selector/app/business-info-page-locator'

class BusinessInfo {

    async inputBusinessName(businessName) {
        await t.typeText(businessInfoPageLocator.inputBusinessName, businessName)
    }

    async inputBrandName(brandName) {
        await t.typeText(businessInfoPageLocator.inputBrandName, brandName)
    }

    async selectBusinessType(businessType) {
        await t.click(businessInfoPageLocator.inputBusinessType).click(businessInfoPageLocator.selectDropDownList.withText(businessType))
    }

    async selectBusinessSizeTypes(businessSizeType) {
        await t.click(businessInfoPageLocator.inputBusinessSizeTypes).click(businessInfoPageLocator.selectDropDownList.withText(businessSizeType))
    }

    async SelectBusinessCategory(businessCategory) {
        await t.click(businessInfoPageLocator.inputBusinessCategory).click(businessInfoPageLocator.selectDropDownList.withText(businessCategory))
    }

    async uploadProfilePhoto(filePath) {
        await t.setFilesToUpload(businessInfoPageLocator.chooseFile, filePath)
    }

    async inputOwnerName(ownerName) {
        await t.typeText(businessInfoPageLocator.inputOwnerName, ownerName)
    }

    async inputOwnerKTPNumber(ktpNumber) {
        await t.typeText(businessInfoPageLocator.inputOwnerKTPNumber, ktpNumber)
    }

    async inputFinanceName(financeName) {
        await t.typeText(businessInfoPageLocator.inputFinanceName, financeName)
    }

    async inputFinancePhoneNumber(financePhoneNumber) {
        await t.typeText(businessInfoPageLocator.inputFinancePhoneNumber, financePhoneNumber)
    }

    async inputFinanceEmail(financeEmail) {
        await t.typeText(businessInfoPageLocator.inputFinanceEmail, financeEmail)
    }

    async inputNumberOfStores(stores) {
        await t.click(businessInfoPageLocator.inputNumberofStores).pressKey('ctrl+a delete').typeText(businessInfoPageLocator.inputNumberofStores, stores)
    }

    async selectRegisteredCountry(registeredCountry) {
        await t.click(businessInfoPageLocator.inputCountry).click(businessInfoPageLocator.selectDropDownList.withText(registeredCountry))
    }

    async selectRegisteredProvince(registeredProvince) {
        await t.click(businessInfoPageLocator.inputProvince).click(businessInfoPageLocator.selectDropDownList.withText(registeredProvince))
    }

    async selectRegisteredCity(registeredCity) {
        await t.click(businessInfoPageLocator.inputCity).click(businessInfoPageLocator.selectDropDownList.withText(registeredCity))
    }

    async selectRegisteredArea(registeredArea) {
        await t.click(businessInfoPageLocator.inputArea).click(businessInfoPageLocator.selectDropDownList.withText(registeredArea))
    }

    async inputRegisteredAddress(registeredAddress) {
        await t.typeText(businessInfoPageLocator.inputAddress, registeredAddress)
    }

    async inputRegisteredPostCode(registeredPostCode) {
        await t.typeText(businessInfoPageLocator.inputPostCode, registeredPostCode)
    }

    async clickSameAddressCheckbox() {
        await t.click(businessInfoPageLocator.sameAddressCheckbox)
    }

    async selectOfficeCountry(officeCountry) {
        await t.click(businessInfoPageLocator.inputOfficeCountry).click(businessInfoPageLocator.selectDropDownList.withText(officeCountry))
    }

    async selectOfficeProvince(officeProvince) {
        await t.click(businessInfoPageLocator.inputOfficeProvince).click(businessInfoPageLocator.selectDropDownList.withText(officeProvince))
    }

    async selectOfficeCity(officeCity) {
        await t.click(businessInfoPageLocator.inputOfficeCity).click(businessInfoPageLocator.selectDropDownList.withText(officeCity))
    }

    async selectOfficeArea(officeArea) {
        await t.click(businessInfoPageLocator.inputOfficeArea).click(businessInfoPageLocator.selectDropDownList.withText(officeArea))
    }

    async inputOfficeAddress(officeAddress) {
        await t.typeText(businessInfoPageLocator.inputOfficeAddress, officeAddress)
    }

    async inputOfficePostCode(officePostCode) {
        await t.typeText(businessInfoPageLocator.inputOfficePostCode, officePostCode)
    }

    async inputContactName(contactName) {
        await t.typeText(businessInfoPageLocator.inputContactName, contactName)
    }

    async inputContactMobileNumber(contactMobileNumber) {
        await t.typeText(businessInfoPageLocator.inputContactMobileNumber, contactMobileNumber)
    }

    async inputContactTelephone(contactTelephone) {
        await t.typeText(businessInfoPageLocator.inputContactTelephone, contactTelephone)
    }

    async inputContactEmailAddress(contactEmailAddress) {
        await t.typeText(businessInfoPageLocator.inputContactEmailAddress, contactEmailAddress)
    }

    async selectBankName(bankName) {
        await t.click(businessInfoPageLocator.inputBankName).click(businessInfoPageLocator.selectDropDownList.withText(bankName))
    }

    async inputSubsidiaryBankName(subsidiaryBankName) {
        await t.typeText(businessInfoPageLocator.inputSubsidiaryBankName, subsidiaryBankName)
    }

    async inputAccountNumber(accountNumber) {
        await t.typeText(businessInfoPageLocator.inputAccountNumber, accountNumber)
    }

    async inputCardholderName(cardholderName) {
        await t.typeText(businessInfoPageLocator.inputCardholderName, cardholderName)
    }

    async clickBackButton() {
        await t.click(businessInfoPageLocator.backButton)
    }

    async clickContinueButton() {
        await t.click(businessInfoPageLocator.continueButton)
    }

    async clickEditButton() {
        await t.click(businessInfoPageLocator.editButton)
    }

    async clickOkButton() {
        await t.click(businessInfoPageLocator.okPopUpButton)
    }

    async normalFlowBusinessInfo(data) {
        this.inputBusinessName(data.businessProfile.businessName)
        this.inputBrandName(data.businessProfile.brandName)
        this.selectBusinessType(data.businessProfile.businessType)
        this.selectBusinessSizeTypes(data.businessProfile.businessSizeType)
        this.SelectBusinessCategory(data.businessProfile.businessCategory)
        this.uploadProfilePhoto(data.businessProfile.filePath)
        this.inputOwnerName(data.businessProfile.ownerName)
        this.inputOwnerKTPNumber(data.businessProfile.ktpNumber)
        this.inputFinanceName(data.businessProfile.financeName)
        this.inputFinancePhoneNumber(data.businessProfile.financePhoneNumber)
        this.inputFinanceEmail(data.businessProfile.financeEmail)
        this.inputNumberOfStores(data.businessProfile.stores)
        this.clickContinueButton()
        this.selectRegisteredCountry(data.officeAddress.registeredCountry)
        this.selectRegisteredProvince(data.officeAddress.registeredProvince)
        this.selectRegisteredCity(data.officeAddress.registeredCity)
        this.selectRegisteredArea(data.officeAddress.registeredArea)
        this.inputRegisteredAddress(data.officeAddress.registeredAddress)
        this.inputRegisteredPostCode(data.officeAddress.registeredPostCode)
        this.clickContinueButton()
        // this.clickSameAddressCheckbox()
        // await t.debug()
        // this.selectOfficeCountry(data.officeAddress.officeCountry)
        // this.selectOfficeProvince(data.officeAddress.officeProvince)
        // this.selectOfficeCity(data.officeAddress.officeCity)
        // this.selectOfficeArea(data.officeAddress.officeArea)
        // this.inputOfficeAddress(data.officeAddress.officeAddress)
        // this.inputOfficePostCode(data.officeAddress.officePostCode)
        this.inputContactName(data.contactInfo.contactName)
        this.inputContactMobileNumber(data.contactInfo.contactMobileNumber)
        this.inputContactTelephone(data.contactInfo.contactTelephone)
        this.inputContactEmailAddress(data.contactInfo.contactEmailAddress)
        this.clickContinueButton()
        this.selectBankName(data.settleAccountInfo.bankName)
        this.inputSubsidiaryBankName(data.settleAccountInfo.subsidiaryBankName)
        this.inputAccountNumber(data.settleAccountInfo.accountNumber)
        this.inputCardholderName(data.settleAccountInfo.cardholderName)
        this.clickContinueButton()
        this.clickOkButton()
    }
}

export default new BusinessInfo()