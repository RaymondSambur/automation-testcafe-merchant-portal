import mailinatorLocator from '../../page-selector/app/mailinator-page-locator'
import { t } from 'testcafe'

class Mailinator {
    async activationLinkMP(userEmail) {
        await t.typeText(mailinatorLocator.searchBoxInput, userEmail, { paste: true, speed: 0.3 })
        await t.click(mailinatorLocator.goBtn)
        await t.click(mailinatorLocator.checkEmailVerification, { speed: 0.2 })
        await t.switchToIframe(mailinatorLocator.bodyEmail)
        await t.click(mailinatorLocator.activateLink)
    }
}

export default new Mailinator()