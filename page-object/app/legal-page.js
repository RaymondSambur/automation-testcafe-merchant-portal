import { t } from "testcafe"
import legalPageLocator from "../../page-selector/app/legal-page-locator"

class Legal {
    async uploadAktaAnggaranButton(aktaAnggaran) {
        await t.setFilesToUpload(legalPageLocator.aktaAnggaranButton, aktaAnggaran)
    }

    async uploadSiupButton(siup) {
        await t.setFilesToUpload(legalPageLocator.siupButton, siup)
    }

    async uploadNpwpButton(npwp) {
        await t.setFilesToUpload(legalPageLocator.npwpButton, npwp)
    }

    async uploadRekeningKoranButton(rekeningKoran) {
        await t.setFilesToUpload(legalPageLocator.rekeningKoranButton, rekeningKoran)
    }

    async uploadKtpButton(ktp) {
        await t.setFilesToUpload(legalPageLocator.ktpButton, ktp)
    }

    async uploadNibButton(nib) {
        await t.setFilesToUpload(legalPageLocator.nibButton, nib)
    }

    async clickDeleteAktaAnggaranButton() {
        await t.click(legalPageLocator.deleteAktaAnggaranButton)
    }

    async clickDeleteSiupButton() {
        await t.click(legalPageLocator.deleteSiupButton)
    }

    async clickDeleteNpwpButton() {
        await t.click(legalPageLocator.deleteNPWPButton)
    }

    async clickDeleteRekeningKoranButton() {
        await t.click(legalPageLocator.deleteRekeningKoranButton)
    }

    async clickDeleteKtpButton() {
        await t.click(legalPageLocator.deleteKtpButton)
    }

    async clickDeleteNibButton() {
        await t.click(legalPageLocator.deleteNibButton)
    }

    async clickTncCheckbox() {
        await t.click(legalPageLocator.tncCheckbox)
    }

    async clickCancelButton() {
        await t.click(legalPageLocator.cancelTncButton)
    }

    async clickOkButton() {
        await t.click(legalPageLocator.okTncButton)
    }

    async clickExitButton() {
        await t.click(legalPageLocator.exitTncButton)
    }

    async clickSubmitButton() {
        await t.click(legalPageLocator.submitButton)
    }

    async clickBackToMainPageButton() {
        await t.click(legalPageLocator.backToMainPageButton)
    }
}
export default new Legal()