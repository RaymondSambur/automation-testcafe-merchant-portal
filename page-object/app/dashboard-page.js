import { t } from "testcafe"
import dashboardPageLocator from "../../page-selector/app/dashboard-page-locator"

class Dashboard {
    async clickRegisterBusinessButton() {
        await t.click(dashboardPageLocator.registerBusinessButton)
    }

    async clickLegalDocButton() {
        await t.click(dashboardPageLocator.legalDocButton)
    }

    async clickDashboardButton() {
        await t.click(dashboardPageLocator.dashboardButton)
    }

    async clickApiDocsButton() {
        await t.click(dashboardPageLocator.apiDocsButton)
    }

    async clickMerchantButton() {
        await t.click(dashboardPageLocator.merchantButton)
    }

    async clickLoyaltyButton() {
        await t.click(dashboardPageLocator.loyaltyButton)
    }

    async clickDanaBoostButton() {
        await t.click(dashboardPageLocator.danaBoostButton)
    }

    async clickSettingButton() {
        await t.click(dashboardPageLocator.settingsButton)
    }

    async clickLogoutButton() {
        await t.click(dashboardPageLocator.logoutButton)
    }

    async clickBusinessInfoButton() {
        await t.click(dashboardPageLocator.businessInfoButton)
    }

    async clickLoyaltyDashboardButton() {
        await t.click(dashboardPageLocator.loyaltyDashboardButton)
    }

    async clickLoyaltyDetailButton() {
        await t.click(dashboardPageLocator.loyaltyDetailButton)
    }

    async clickUserLoyaltyButton() {
        await t.click(dashboardPageLocator.userLoyaltyButton)
    }

    async clickPointsActivity() {
        await t.click(dashboardPageLocator.pointsActivity)
    }

    async clickChangePasswordButton() {
        await t.click(dashboardPageLocator.changePasswordButton)
    }

}
export default new Dashboard()