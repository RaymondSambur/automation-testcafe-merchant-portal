import registerLocator from "../../page-selector/app/register-page-locator"
import { t } from "testcafe"

class Register {

    async normalRegister(data) {
        this.inputBusinessBrand(data.brandName)
        this.inputOwnerName(data.ownerName)
        this.selectProvince(data.province)
        this.selectBusinessCategory(data.businessCategory)
        this.inputEmailAddress(data.email)
        this.inputPassword(data.password)
        this.inputConfirmPassword(data.passwordConfirm)
        this.selectBusinessChannel(data.businessChannel)
        this.selectUsingDana(data.danaProduct, data.inputDanaProduct)
        this.clickContinueButton()

    }

    async inputBusinessBrand(businessBrand) {
        await t.typeText(registerLocator.businessBrandInput, businessBrand)
    }

    async inputOwnerName(ownerName) {
        await t.typeText(registerLocator.ownerNameInput, ownerName)
    }

    async selectProvince(province) {
        await t.click(registerLocator.selectProvince).click(registerLocator.customSelectProvince.withText(province))
    }

    async selectBusinessCategory(businessCategory) {
        await t.click(registerLocator.selectBusinessCategory).click(registerLocator.customSelectBusinessCategory.withText(businessCategory))
    }

    async inputEmailAddress(email) {
        await t.typeText(registerLocator.emailAddress, email)
        console.log(email)
    }

    async inputPassword(password) {
        await t.typeText(registerLocator.passInput, password)
    }

    async inputConfirmPassword(confirmPassword) {
        await t.typeText(registerLocator.passConfirm, confirmPassword)
    }

    async selectBusinessChannel(businessChannel) {
        switch (businessChannel) {
            case "Offline":
                await t.click(registerLocator.businessChannelOffline)
            case "Online":
                await t.click(registerLocator.businessChannelOnline)
        }
    }

    async selectUsingDana(usingDana, input) {
        switch (usingDana) {
            case "Yes":
                await t.click(registerLocator.usingDanaYes).typeText(registerLocator.usingDanaInput, input)
            case "No":
                await t.click(registerLocator.usingDanaNo)
        }
    }

    async clickContinueButton() {
        await t.click(registerLocator.continueBtn).click(registerLocator.okBtnPopUpLinkSent, { speed: 0.2 })
    }

}

export default new Register()