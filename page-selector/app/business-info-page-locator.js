import { Selector } from "testcafe"
import xPathToCss from "xpath-to-css"
import xPathSelector from "../../xpath-selector"

class BusinessInfoLocator {
    constructor() {
        //Business Profile Section
        this.inputBusinessName = Selector("#businessNameInput")
        this.inputBrandName = Selector("#brandNameInput")
        this.inputBusinessType = Selector("#businessTypeInput")
        this.inputBusinessSizeTypes = Selector('#businessSizeInput')
        this.inputBusinessCategory = Selector('#mccInput')
        this.chooseFile = Selector('input').withAttribute('type', 'file')
        this.chooseFileBtn = Selector('.ant-btn')
        this.inputOwnerName = Selector('#ownerNameInput')
        this.inputOwnerKTPNumber = Selector('#ownerKtpNumberInput')
        this.inputFinanceName = Selector('#financeNameInput')
        this.inputFinancePhoneNumber = Selector('#financePhoneNumberInput')
        this.inputFinanceEmail = Selector('#financeEmailInput')
        this.inputNumberofStores = Selector('#numberOfStoresInput')

        //Office Address Section
        this.inputCountry = Selector("#registeredCountryInput")
        this.inputProvince = Selector("#registeredProvinceInput")
        this.inputCity = Selector("#registeredCityInput")
        this.inputArea = Selector("#registeredAreaInput")
        this.inputAddress = Selector("#registeredAddressInput")
        this.inputPostCode = Selector("#registeredPostCodeInput")
        this.sameAddressCheckbox = Selector("#sameAddressInput")
        this.inputOfficeCountry = Selector("#officeCountryInput")
        this.inputOfficeProvince = Selector("#officeProvinceInput")
        this.inputOfficeCity = Selector("#officeCityInput")
        this.inputOfficeArea = Selector("#officeAreaInput")
        this.inputOfficeAddress = Selector("#officeAddressInput")
        this.inputOfficePostCode = Selector("#officePostCodeInput")

        //Contact Info Section
        this.inputContactName = Selector('#contactOfficialNameInput')
        this.inputContactMobileNumber = Selector('#contactMobilePhoneInput')
        this.inputContactTelephone = Selector('#contactTelephoneInput')
        this.inputContactEmailAddress = Selector('#contactEmailInput')

        //Settle Account Info Section
        this.inputBankName = Selector('#bankNameInput')
        this.inputSubsidiaryBankName = Selector('#subsidiaryBankNameInput')
        this.inputAccountNumber = Selector('#accountNumberInput')
        this.inputCardholderName = Selector('#cardholderNameInput')

        //General Use
        this.selectDropDownList = Selector('.ant-select-dropdown-menu-item')
        this.continueButton = Selector(xPathToCss("//button[@class='ant-btn ant-btn-primary ant-btn-lg']"))
        this.backButton = Selector(xPathToCss("//button[@class='ant-btn ant-btn-default ant-btn-lg']"))
        this.okPopUpButton = Selector(xPathSelector("//button/span[contains(text(),'OK')]"))

        //Condition
        this.editButton = Selector(xPathSelector("//button/span[contains(text(),'Edit Business Info')]"))
    }
}

export default new BusinessInfoLocator()