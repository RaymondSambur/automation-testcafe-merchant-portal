import { Selector } from "testcafe"
import xPathToCss from "xpath-to-css"
import xPathSelector from "../../xpath-selector"

class DashboardLocator {
    constructor() {
        //Newly created account
        this.registerBusinessButton = Selector(xPathSelector("//button/span[contains(text(),'Register Business')]"))
        this.legalDocButton = Selector(xPathSelector("//button/span[contains(text(),'Go to Legal Docs Page')]"))

        //Sidebar menus
        this.dashboardButton = Selector(xPathSelector("//ul/li/span[contains(text(),'Dashboard')]"))
        this.apiDocsButton = Selector(xPathSelector("//ul/li/span[contains(text(),'API Docs')]"))
        this.merchantButton = Selector(xPathSelector("//ul/li/div/span/span[contains(text(),'Merchant')]"))
        this.loyaltyButton = Selector(xPathSelector("//ul/li/div/span/span[contains(text(),'Loyalty')]"))
        this.danaBoostButton = Selector(xPathSelector("//ul/li/a/span/span[contains(text(),'DANA Boost')]"))
        this.settingsButton = Selector(xPathSelector("//ul/li/div/span/span[contains(text(),'Settings')]"))
        this.logoutButton = Selector(xPathSelector("//ul/li/a/span[contains(text(),'Log Out')]"))

        //Sidebar submenus
        this.businessInfoButton = Selector(xPathSelector("//ul/li/a/span[contains(text(),'Business Info')]"))
        this.loyaltyDashboardButton = Selector(xPathSelector("//ul/li/a/span[contains(text(),'Dashboard')]"))
        this.loyaltyDetailButton = Selector(xPathSelector("//ul/li/a/span[contains(text(),'Loyalty Detail')]"))
        this.userLoyaltyButton = Selector(xPathSelector("//ul/li/a/span[contains(text(),'User Loyalty')]"))
        this.pointsActivity = Selector(xPathSelector("//ul/li/a/span[contains(text(),'Points Activity')]"))
        this.changePasswordButton = Selector(xPathSelector("//ul/li/a/span[contains(text(),'Change Password')]"))

        this.hideSidebarMenuButton = Selector(xPathSelector("//div[@class='ant-layout-sider-trigger']"))
    }
}

export default new DashboardLocator()