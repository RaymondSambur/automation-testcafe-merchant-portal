import { Selector } from "testcafe"
import xPathToCss from "xpath-to-css"
import xPathSelector from "../../xpath-selector"

class LoginLocator {
    constructor() {
        this.emailInput = Selector('#loginEmailInput')
        this.passwordInput = Selector('#loginPasswordInput')
        this.loginBtn = Selector(xPathToCss("//button[@class='ant-btn menu-login-button registration-login-button ant-btn-primary']"))
        this.forgotPassswordBtn = Selector(xPathToCss("//a[@class='registration-login-forgot']"))
        this.rememberMeBtn = Selector(xPathSelector("//span[contains(text(),'Remember me')]"))
    }
}

export default new LoginLocator()