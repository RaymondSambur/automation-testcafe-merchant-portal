import { Selector } from "testcafe"
import xPathToCss from "xpath-to-css"
import xPathSelector from "../../xpath-selector"

class LegalLocator {
    constructor() {
        this.aktaAnggaranButton = Selector(xPathSelector("//li[1]//div[2]//span[1]//div[1]//span[1]//input[1]")).withAttribute('type', 'file')
        this.siupButton = Selector(xPathSelector("//li[2]//div[2]//span[1]//div[1]//span[1]//input[1]")).withAttribute('type', 'file')
        this.npwpButton = Selector(xPathSelector("//li[3]//div[2]//span[1]//div[1]//span[1]//input[1]")).withAttribute('type', 'file')
        this.rekeningKoranButton = Selector(xPathSelector("//li[4]//div[2]//span[1]//div[1]//span[1]//input[1]")).withAttribute('type', 'file')
        this.ktpButton = Selector(xPathSelector("//li[5]//div[2]//span[1]//div[1]//span[1]//input[1]")).withAttribute('type', 'file')
        this.nibButton = Selector(xPathSelector("//li[6]//div[2]//span[1]//div[1]//span[1]//input[1]")).withAttribute('type', 'file')

        this.deleteAktaAnggaranButton = Selector(xPathSelector("//li[1]//div[2]//span[1]//div[2]//div[1]//span[1]//div[1]//div[1]//span[1]//span[2]//a[1]//i[1]"))
        this.deleteSiupButton = Selector(xPathSelector("//li[2]//div[2]//span[1]//div[2]//div[1]//span[1]//div[1]//div[1]//span[1]//span[2]//a[1]//i[1]"))
        this.deleteNPWPButton = Selector(xPathSelector("//li[3]//div[2]//span[1]//div[2]//div[1]//span[1]//div[1]//div[1]//span[1]//span[2]//a[1]//i[1]"))
        this.deleteRekeningKoranButton = Selector(xPathSelector("//li[4]//div[2]//span[1]//div[2]//div[1]//span[1]//div[1]//div[1]//span[1]//span[2]//a[1]//i[1]"))
        this.deleteKtpButton = Selector(xPathSelector("//li[5]//div[2]//span[1]//div[2]//div[1]//span[1]//div[1]//div[1]//span[1]//span[2]//a[1]//i[1]"))
        this.deleteNibButton = Selector(xPathSelector("//li[6]//div[2]//span[1]//div[2]//div[1]//span[1]//div[1]//div[1]//span[1]//span[2]//a[1]//i[1]"))

        this.tncCheckbox = Selector(xPathToCss("//input[@class='ant-checkbox-input']"))
        this.cancelTncButton = Selector(xPathSelector("//button/span[contains(text(),'Cancel')]"))
        this.okTncButton = Selector(xPathSelector("//div[@class='ant-modal-footer']//div//span[contains(text(),'OK')]"))
        this.exitTncButton = Selector(xPathToCss("//div[@class='ant-modal-wrap onboard-tnc-modal']//i[@class='anticon anticon-close ant-modal-close-icon']"))

        this.submitButton = Selector(xPathToCss("//button[@class='ant-btn action-button ant-btn-primary']"))
        this.backToMainPageButton = Selector(xPathSelector("//span[contains(text(),'< Back to Main Page')]"))
    }
}

export default new LegalLocator()