import { Selector } from "testcafe"
import xPathToCss from "xpath-to-css"
import XPathSelector from "../../xpath-selector"

class MailinatorLocator {
    constructor() {
        this.searchBoxInput = Selector('#inbox_field')
        // this.searchBoxInput = Selector(xPathToCss("//input[@id='inbox_field']"))
        this.goBtn = Selector('#go_inbox')

        this.searchBoxInput = Selector('.form-control')
        this.goBtn = Selector('#go_inbox')
        this.checkEmailVerification = Selector(XPathSelector("//td[contains(text(),'mp-bot@dana.id')]"))
        // this.checkEmailVerification = Selector(xPathToCss("//td[contains(text(),'mp-bot@dana.id')]"))
        this.bodyEmail = Selector('#msg_body')
        this.activateLink = Selector('[title="Activation"]')
    }
}

export default new MailinatorLocator()