import { Selector, t } from 'testcafe'
import xPathToCss from 'xpath-to-css'
import xPathSelector from '../../xpath-selector'

class RegisterLocator {
    constructor() {
        this.businessBrandInput = Selector('#businessBrandInput')
        this.ownerNameInput = Selector('#ownerNameInput')
        this.selectProvince = Selector('#provinceInput')
        // this.verifyProvince = Selector(XPathSelector('//div[@id=provinceInput]/div[1]'))
        this.customSelectProvince = Selector('.ant-select-dropdown-menu-item')
        this.selectBusinessCategory = Selector('#businessCategoryInput')
        this.customSelectBusinessCategory = Selector('.ant-select-dropdown-menu-item')
        this.emailAddress = Selector('#emailInput')
        this.passInput = Selector('#passwordInput')
        this.passConfirm = Selector('#confirmInput')
        this.businessChannelOffline = Selector(xPathSelector("//span[contains(text(),'Offline')]"))
        this.businessChannelOnline = Selector(xPathSelector("//span[contains(text(),'Online')]"))
        // this.businessChannelOffline = Selector(".ant-checkbox-group-item").withText("Offline")
        // this.businessChannelOnline = Selector(".ant-checkbox-group-item").withText("Online")
        this.usingDanaYes = Selector(xPathToCss("//span[@class='ant-radio']//input[@class='ant-radio-input']")) //default no
        this.usingDanaNo = Selector(xPathToCss("//span[@class='ant-radio ant-radio-checked']//input[@class='ant-radio-input']")) //yes
        this.usingDanaInput = Selector("#danaProductInput")
        this.continueBtn = Selector(xPathToCss("//button[@class='ant-btn registration-submit-button ant-btn-primary']"))
        this.okBtnPopUpLinkSent = Selector(xPathToCss("//button[@class='ant-btn modal-button-registration-ok ant-btn-primary']"))

        ////button[@class='ant-btn modal-button-registration-ok ant-btn-primary']
        //ant-btn modal-button-registration-ok ant-btn-primary
        //div.ant-modal-wrap.ant-modal-centered div.ant-modal div.ant-modal-content:nth-child(2) div.ant-modal-body div.ant-row.modal-row:nth-child(4) > button.ant-btn.modal-button-registration-ok.ant-btn-primary
        ///html[1]/body[1]/div[5]/div[1]/div[2]/div[1]/div[2]/div[1]/div[4]/button[1]


        //Error Messages
        this.errorBusinessBrand = Selector("#section.ant-layout div.auth-container div.auth-register-container main.app-layout-content.ant-layout-content div.main div.container form.ant-form.ant-form-horizontal.custom.registration-form div.box-list div.ant-row:nth-child(3) div.ant-col.ant-col-xs-24.ant-col-sm-24.ant-col-md-24.ant-col-lg-18.ant-col-lg-offset-3 div.ant-row.ant-form-item.ant-form-item-with-help div.ant-col.ant-form-item-control-wrapper:nth-child(2) div.ant-form-item-control.has-error > div.ant-form-explain")
        this.errorOwnerName = Selector("#section.ant-layout div.auth-container div.auth-register-container main.app-layout-content.ant-layout-content div.main div.container form.ant-form.ant-form-horizontal.custom.registration-form div.box-list div.ant-row:nth-child(4) div.ant-col.ant-col-xs-24.ant-col-sm-24.ant-col-md-24.ant-col-lg-18.ant-col-lg-offset-3 div.ant-row.ant-form-item.ant-form-item-with-help div.ant-col.ant-form-item-control-wrapper:nth-child(2) div.ant-form-item-control.has-error > div.ant-form-explain")
        this.errorProvince = Selector("#section.ant-layout div.auth-container div.auth-register-container main.app-layout-content.ant-layout-content div.main div.container form.ant-form.ant-form-horizontal.custom.registration-form div.box-list div.ant-row:nth-child(5) div.ant-col.ant-col-xs-24.ant-col-sm-24.ant-col-md-24.ant-col-lg-18.ant-col-lg-offset-3 div.ant-row.ant-form-item.ant-form-item-with-help div.ant-col.ant-form-item-control-wrapper:nth-child(2) div.ant-form-item-control.has-error > div.ant-form-explain")
        this.errorBusinessCategory = Selector("#section.ant-layout div.auth-container div.auth-register-container main.app-layout-content.ant-layout-content div.main div.container form.ant-form.ant-form-horizontal.custom.registration-form div.box-list div.ant-row:nth-child(6) div.ant-col.ant-col-xs-24.ant-col-sm-24.ant-col-md-24.ant-col-lg-18.ant-col-lg-offset-3 div.ant-row.ant-form-item.ant-form-item-with-help div.ant-col.ant-form-item-control-wrapper:nth-child(2) div.ant-form-item-control.has-error > div.ant-form-explain")
        this.errorEmailAddress = Selector("#section.ant-layout div.auth-container div.auth-register-container main.app-layout-content.ant-layout-content div.main div.container form.ant-form.ant-form-horizontal.custom.registration-form div.box-list div.ant-row:nth-child(7) div.ant-col.ant-col-xs-24.ant-col-sm-24.ant-col-md-24.ant-col-lg-18.ant-col-lg-offset-3 div.ant-row.ant-form-item.ant-form-item-with-help div.ant-col.ant-form-item-control-wrapper:nth-child(2) div.ant-form-item-control.has-error > div.ant-form-explain")
        this.errorPassword = Selector("#section.ant-layout div.auth-container div.auth-register-container main.app-layout-content.ant-layout-content div.main div.container form.ant-form.ant-form-horizontal.custom.registration-form div.box-list div.ant-row:nth-child(8) div.ant-col.ant-col-xs-24.ant-col-sm-24.ant-col-md-24.ant-col-lg-18.ant-col-lg-offset-3 div.ant-row.ant-form-item.ant-form-item-with-help div.ant-col.ant-form-item-control-wrapper:nth-child(2) div.ant-form-item-control.has-error > div.ant-form-explain")
        this.errorPasswordConfirm = Selector("#section.ant-layout div.auth-container div.auth-register-container main.app-layout-content.ant-layout-content div.main div.container form.ant-form.ant-form-horizontal.custom.registration-form div.box-list div.ant-row:nth-child(9) div.ant-col.ant-col-xs-24.ant-col-sm-24.ant-col-md-24.ant-col-lg-18.ant-col-lg-offset-3 div.ant-row.ant-form-item.ant-form-item-with-help div.ant-col.ant-form-item-control-wrapper:nth-child(2) div.ant-form-item-control.has-error > div.ant-form-explain")
        this.errorBusinessChannel = Selector("#section.ant-layout div.auth-container div.auth-register-container main.app-layout-content.ant-layout-content div.main div.container form.ant-form.ant-form-horizontal.custom.registration-form div.box-list div.ant-row:nth-child(10) div.ant-col.ant-col-xs-24.ant-col-sm-24.ant-col-md-24.ant-col-lg-18.ant-col-lg-offset-3 div.ant-row.ant-form-item.ant-form-item-with-help div.ant-col.ant-form-item-control-wrapper:nth-child(2) div.ant-form-item-control.has-error > div.ant-form-explain")

    }
}

export default new RegisterLocator()