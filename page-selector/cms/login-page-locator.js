import { Selector } from "testcafe"
import xPathToCss from "xpath-to-css"
import xPathSelector from "../../xpath-selector"

class LoginLocator {
    constructor() {
        this.inputEmail = Selector("#emailInput")
        this.inputPassword = Selector("#passwordInput")
        this.rememberMeButton = Selector("#rememberMeInput")
        this.hideShowButton = Selector(xPathSelector("//i[@class='anticon anticon-lock black-transparent-icon']"))
        this.loginButton = Selector(xPathSelector("//span[contains(text(),'LOGIN')]"))
    }
}

export default new LoginLocator()