import { Selector } from "testcafe"
import xPathToCss from "xpath-to-css"
import xPathSelector from "../../xpath-selector"

class DashboardLocator {
    constructor() {
        //Sidebar Menu
        this.banksButton = Selector(xPathSelector("//li[contains(@class,'ant-menu-item-group')]//li[1]//a[1]"))
        this.merchantButton = Selector(xPathToCss("//li[1]//ul[1]//li[2]//div[1]"))
        this.notificationButton = Selector(xPathSelector("//span[contains(text(),'Notification')]"))
        this.eazyEatsButton = Selector(xPathSelector("//span[contains(text(),'EAZY Eats')]"))
        this.articleButton = Selector(xPathToCss("//li[2]//ul[1]//li[1]//div[1]"))
        this.categoryButton = Selector(xPathToCss("//li[2]//ul[1]//li[2]//div[1]"))
        this.userButton = Selector(xPathToCss("//li[3]//ul[1]//li[1]//div[1]"))
        this.roleButton = Selector(xPathSelector("//div[@class='ant-menu-submenu-title']//span//span[contains(text(),'Role')]"))
        this.tickerButton = Selector(xPathSelector("//span[contains(text(),'Ticker')]"))
        this.logoutButton = Selector(xPathSelector("//span[contains(text(),'Log Out')]"))

        //Sub Menus
        this.leadButton = Selector(xPathToCss("//ul[@id='merchant$Menu']//li[1]//a[1]"))
        this.applicantButton = Selector(xPathToCss("//ul[@id='merchant$Menu']//li[2]//a[1]"))
        this.integrationButton = Selector(xPathToCss("//ul[@id='merchant$Menu']//li[3]//a[1]"))
        this.merchantListButton = Selector(xPathSelector("//span[contains(text(),'Merchant List')]"))
        this.createMerchantButton = Selector(xPathSelector("//span[contains(text(),'Create New Merchant')]"))
        this.activationButton = Selector(xPathSelector("//span[contains(text(),'Activation')]"))
        this.crowdButton = Selector(xPathSelector("//span[contains(text(),'Crowd')]"))
        this.shopButton = Selector(xPathSelector("//span[contains(text(),'Shop')]"))
        this.articleListButton = Selector(xPathSelector("//span[contains(text(),'Article List')]"))
        this.createNewArticleButton = Selector(xPathSelector("//span[contains(text(),'Create New Article')]"))
        this.categoryList = Selector(xPathSelector("//span[contains(text(),'Category List')]"))
        this.createNewCategoryButton = Selector(xPathSelector("//span[contains(text(),'Create New Category')]"))

    }
}