import config from "../config.json"
import registerPage from "../page-object/app/register-page"
import mailinatorPage from "../page-object/app/mailinator-page"
import loginPage from "../page-object/app/login-page"
import businessInfoPage from "../page-object/app/business-info-page"
import dashboardPage from "../page-object/app/dashboard-page"
import legalPage from "../page-object/app/legal-page"
import defaultData from "../default-data"

fixture("Register New Account").page(config.test02_env)
test('testing register', async t => {
    await registerPage.normalRegister(defaultData.registerApp)
    // await t.expect(registerPageLocator.businessBrandInput.value).eql(defaultData.brandName)
    // await t.expect(registerPageLocator.ownerNameInput.value).eql(defaultData.ownerName)
    // await t.expect(registerPageLocator.emailAddress.value).eql(defaultData.email)
    // await t.expect(registerPageLocator.passInput.value).eql(defaultData.password)
    // await t.expect(registerPageLocator.passConfirm.value).eql(defaultData.passwordConfirm)
    await t.wait(5000)
})

fixture("Activate mailinator").page(config.mailinator)
test('testing mailinator function', async t => {
    await mailinatorPage.activationLinkMP(defaultData.registerApp.email)
    await t.wait(5000)
})

fixture("Login with new account & filling Business Info").page(config.test02_env)
test('testing login function', async t => {
    await loginPage.normalLogin(defaultData.registerApp)
    await dashboardPage.clickMerchantButton()
    await dashboardPage.clickBusinessInfoButton()
    await businessInfoPage.normalFlowBusinessInfo(defaultData.businessInfoApp)
    await legalPage.uploadAktaAnggaranButton(defaultData.legalApp.aktaAnggaran)
    await legalPage.uploadSiupButton(defaultData.legalApp.siup)
    await legalPage.uploadNpwpButton(defaultData.legalApp.npwp)
    await legalPage.uploadRekeningKoranButton(defaultData.legalApp.rekeningKoran)
    await legalPage.uploadKtpButton(defaultData.legalApp.ktp)
    await legalPage.uploadNibButton(defaultData.legalApp.nib)
    await legalPage.clickTncCheckbox()
    await legalPage.clickOkButton()
    await legalPage.clickSubmitButton()
    await t.wait(5000)
})